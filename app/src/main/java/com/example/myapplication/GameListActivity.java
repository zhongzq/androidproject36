package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.adapter.BaseRecyclerAdapter;
import com.example.myapplication.adapter.GameListAdapter;
import com.example.myapplication.model.GameList;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Zizhuo Tian & ZongQiZhong
 */

public class GameListActivity extends BaseActivity {

    RecyclerView recyclerView;
    GameListAdapter adapter;
    List<String> filenameList;
    List<GameList> list;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gamelist);
        filenameList = getListFilesName(getFilesDir());
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        adapter = new GameListAdapter(this);
        LinearLayoutManager linearManager = new LinearLayoutManager(this);
        linearManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearManager);
        adapter.addDatas(initDate());
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener<GameList>() {
            @Override
            public void onItemClick(int position, GameList data) {
                Intent intent = new Intent(GameListActivity.this, HistoryActivity.class);
                intent.putExtra(INTENT_DATA_KEY_FILENAME, data.getFileName());
                startActivity(intent);
            }
        });


    }
    List<GameList> initDate(){
        list = new ArrayList<>();
        for (int i = 0; i < filenameList.size(); i++) {
            GameList game = new GameList();
            game.setTitle(getTitleFromFileName(filenameList.get(i)));
            game.setDate(getTMSFromFileName(filenameList.get(i)));
            game.setFileName(filenameList.get(i));
            list.add(game);
        }
        return list;
    }
}
