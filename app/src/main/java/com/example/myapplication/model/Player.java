package com.example.myapplication.model;


/**
 * create player class to distinguish white play and black play
 *
 * @author Zizhuo Tian & ZongQiZhong
 */

public class Player {

    // Color of the player.
    private boolean isWhitePlayer;

    /**
     * @param _isWhitePlayer Identify players
     */
    public Player(boolean _isWhitePlayer) {
        isWhitePlayer = _isWhitePlayer;
    }


    /**
     * @return True if it is a White player, vice versa
     */
    public boolean isWhitePlayer() {
        return isWhitePlayer;
    }


    /**
     * @return Prompt for player to enter moves.
     */
    public String getPrompt() {
        return (isWhitePlayer ? "White" : "Black") + "'s move: ";
    }

}