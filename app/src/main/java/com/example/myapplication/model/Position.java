package com.example.myapplication.model;

/**
 * Defining classes for determining pieces location
 *
 * @author Zizhuo Tian & ZongQiZhong
 */

public class Position {


    // fildIndex : from a to h
    public int fileIdx;

    // rankIndex: from 1 to 8
    public int rankIdx;


    /**
     * Set the position of the chessboard on the chessboard.
     * @param _fileIdx fileIndex
     * @param _rankIdx rankIndex
     */
    public Position(int _fileIdx, int _rankIdx) {
        fileIdx = _fileIdx;
        rankIdx = _rankIdx;
    }
    public Position(Position input) {
        fileIdx = input.fileIdx;
        rankIdx = input.rankIdx;
    }

    /**
     * @param fileIndex File
     * @param rankIndex Rank
     * @return Creating key for each of pieces in the map
     */
    public static String getKey(int fileIndex, int rankIndex) {
        return "" + fileIndex + "-" + rankIndex;
    }

    /**
     * @param obj Same position.
     * @return True if in same position, false otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Position)) {
            return false;
        }
        //
        return this.fileIdx == ((Position)obj).fileIdx && this.rankIdx == ((Position)obj).rankIdx;
    }

    /**
     * @return Key for map of pieces.
     */
    public String getKey() {
        return getKey(fileIdx, rankIdx);
    }



    public String toString() {
        return "" + fileIdx + "" + rankIdx;
    }
}