package com.example.myapplication.model;

/**
 * Defining classes for determining pieces location
 *
 * @author Zizhuo Tian & ZongQiZhong
 */

public class DirectionDistance {
	private int startFile;
	private int startRank;
	private int targetFile;
	private int m_targetRank;

	//Distance between source and target.
	private int distance;

	// the direction of move
	private Direction dir;

	//The change in file for each step.
	private int deltaFile;

	//The change in rank for each step.
	private int deltaRank;


	/**
	 * Represent all the different directions a piece can move.
	 */
	public enum Direction {
		NONE,
		NORTH,
		SOUTH,
		WEST,
		EAST,
		NW,
		NE,
		SW,
		SE
	}


	/**
	 * @param _startFile  Current file.
	 * @param _startRank  Current rank.
	 * @param _targetFile Target file
	 * @param _targetRank Target rank
	 */
	public DirectionDistance(int _startFile, int _startRank, int _targetFile, int _targetRank) {
		dir = Direction.NONE;
		//
		startFile = _startFile;
		startRank = _startRank;
		targetFile = _targetFile;
		m_targetRank = _targetRank;
		//
		int fileDist = _targetFile - _startFile;
		int rankDist = _targetRank - _startRank;
		//
		if (fileDist == 0) {
			distance = Math.abs(rankDist);
			if (rankDist > 0) {
				dir = Direction.NORTH;
				deltaFile = 0;
				deltaRank = 1;
			} else {
				dir = Direction.SOUTH;
				deltaFile = 0;
				deltaRank = -1;
			}
		} else if (rankDist == 0) {
			distance = Math.abs(fileDist);
			if (fileDist > 0) {
				dir = Direction.EAST;
				deltaFile = 1;
				deltaRank = 0;
			} else {
				dir = Direction.WEST;
				deltaFile = -1;
				deltaRank = 0;
			}
		} else if (Math.abs(rankDist) == Math.abs(fileDist)) {
			distance = Math.abs(fileDist);
			if (fileDist > 0 && rankDist > 0) {
				dir = Direction.NE;
				deltaFile = 1;
				deltaRank = 1;
			} else if (fileDist > 0 && rankDist < 0) {
				dir = Direction.SE;
				deltaFile = 1;
				deltaRank = -1;
			} else if (fileDist < 0 && rankDist > 0) {
				dir = Direction.NW;
				deltaFile = -1;
				deltaRank = 1;
			} else {
				dir = Direction.SW;
				deltaFile = -1;
				deltaRank = -1;
			}
		}
	}


	/**
	 * @return Distance between source and target.
	 */
	public int getDistance() {
		return distance;
	}


	/**
	 * @return The change in file for each step
	 */
	public int getDeltaFile() {
		return deltaFile;
	}


	/**
	 * @return The change in rank for each step
	 */
	public int getDeltaRank() {
		return deltaRank;
	}


	/**
	 * @return True if move is valid in every direction, false if move is illegal.
	 */
	public boolean isValid() {
		return dir != Direction.NONE;
	}


	/**
	 * @return True if piece moves along the grid, false otherwise.
	 */
	public boolean isParallel() {
		return dir == Direction.NORTH || dir == Direction.SOUTH || dir == Direction.WEST || dir == Direction.EAST;
	}


	/**
	 * @return True if piece moves diagonally, false otherwise.
	 */
	public boolean isDiagonal() {
		return dir == Direction.NW || dir == Direction.NE || dir == Direction.SW || dir == Direction.SE;
	}


	/**
	 * @return True if valid moves are knight, false otherwise.
	 */
	public boolean isKnightly() {
		return (targetFile == startFile + 1 && m_targetRank == startRank + 2) ||
				(targetFile == startFile + 1 && m_targetRank == startRank - 2) ||
				(targetFile == startFile + 2 && m_targetRank == startRank + 1) ||
				(targetFile == startFile + 2 && m_targetRank == startRank - 1) ||
				(targetFile == startFile - 1 && m_targetRank == startRank + 2) ||
				(targetFile == startFile - 1 && m_targetRank == startRank - 2) ||
				(targetFile == startFile - 2 && m_targetRank == startRank + 1) ||
				(targetFile == startFile - 2 && m_targetRank == startRank - 1);
	}


	/**
	 * @return True if piece moves in all directions 1 step, false otherwise. (King)
	 */
	public boolean isRegal() {
		return isValid() && distance == 1;
	}

	/**
	 * @param isWhite Identifying the color of piece
	 * @return True if the King's side castling move is valid
	 */
	public boolean isCastlingKS(boolean isWhite) {          //no need to concern about file as the king is not moved yet.
		if (isWhite) {
			return startRank == 0 && distance == 2 && dir == Direction.EAST;
		}
		return startRank == 7 && distance == 2 && dir == Direction.EAST;
	}

	/**
	 * @param isWhite Identifying the color of piece
	 * @return True if the Queen's side castling move is valid, false otherwise.
	 */
	public boolean isCastlingQS(boolean isWhite) {
		if (isWhite) {
			return startRank == 0 && distance == 2 && dir == Direction.WEST;
		}
		return startRank == 7 && distance == 2 && dir == Direction.WEST;
	}


	/**
	 * @param isWhite Identifying the color of piece
	 * @return True if pawn moves one step, false otherwise.
	 */
	public boolean isPawOneStep(boolean isWhite) {
		if (isWhite) {
			return dir == Direction.NORTH && distance == 1;
		}
		return dir == Direction.SOUTH && distance == 1;
	}

	/**
	 * @param isWhite Identifying the color of piece
	 * @return True if pawn moves 2 steps, false otherwise.
	 */
	public boolean isPawnTwoStep(boolean isWhite) {
		if (isWhite) {
			return dir == Direction.NORTH && distance == 2 && startRank == 1;
		}
		return dir == Direction.SOUTH && distance == 2 && startRank == 6;
	}

	/**
	 * @param isWhite Identifying the color of piece
	 * @return True if En passant is valid, false otherwise.
	 */
	public boolean isEnPassant(boolean isWhite) {
		if (isWhite) {
			return startRank == 4 && distance == 1 && (dir == Direction.NW || dir == Direction.NE);
		}
		return startRank == 3 && distance == 1 && (dir == Direction.SW || dir == Direction.SE);
	}


	/**
	 * @param isWhite Identifying the color of piece
	 * @return True if pawn is promoted, false otherwise
	 */
	public boolean isPawnPromotion(boolean isWhite) {
		return isWhite ? m_targetRank == 7 : m_targetRank == 0;
	}

	/**
	 * @param isWhite Identifying the color of piece
	 * @return True if pawn is killed
	 */
	public boolean isPawnKill(boolean isWhite) {
		if (isWhite) {
			return distance == 1 && (dir == Direction.NW || dir == Direction.NE);
		}
		return distance == 1 && (dir == Direction.SW || dir == Direction.SE);
	}

}