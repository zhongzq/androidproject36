package com.example.myapplication.model;

/**
 * This class check the state of this game and whether the move succeeds

 *
 * @author Zizhuo Tian & ZongQiZhong
 */

public class Outcome {


    //If it is a successful move
    private boolean success;

    private boolean isOpponentInCheck	= false;

    private boolean isOpponentCheckMate	= false;

    private boolean isOpponentStaleMate	= false;

    private String guiInstruction;

    public String getGuiInstruction() {
        return guiInstruction;
    }

    public void setGuiInstruction(String guiInstruction) {
        this.guiInstruction = guiInstruction;
    }


    // Reason for invalid move
    private String 	reason;

    //If proposing draw
    private boolean isProposingDraw	    = false;

    /**
     * @param inBool True if move is successful
     * @param inString Reason of invalid move
     */
    public Outcome(boolean inBool, String inString) {
        success 	= inBool;
        if(inString.equals("OK")){
            reason = "";
        }
        else {
            reason = inString;
        }
    }

    /**
     * @return True if move is successful
     */
    public boolean isOK() {
        return success;
    }

    /**
     * @return True if opponent is in check after move
     */
    public boolean isOpponentInCheck() {
        return isOpponentInCheck;
    }

    /**
     * Set isOpponentInCheck to true.
     */
    public void setOpponentInCheck() {
        this.isOpponentInCheck = true;
    }


    /**
     * @return True if opponent is in checkmate after move
     */
    public boolean isOpponentCheckMate() {
        return isOpponentCheckMate;
    }

    /**
     * Set isOpponentCheckMate to true.
     */
    public void setOpponentCheckMate() {
        this.isOpponentCheckMate = true;
    }



    /**
     * @return True if opponent is in stalemate after move
     */
    public boolean isOpponentStaleMate() {
        return isOpponentStaleMate;
    }

    /**
     * Set isOpponentStaleMate to true.
     */
    public void setOpponentStaleMate() {
        this.isOpponentStaleMate = true;
    }




    /**
     * @return Reason of invalid move
     */
    public String getReason() {
        return reason;
    }

    /**
     * @return Reason of invalid move
     */
    public void setReason(String input) {
        reason = input;
    }



    /**
     * @return True if it is proposing draw
     */
    public boolean isProposingDraw() {
        return isProposingDraw;
    }

    /**
     * Set isProposingDraw to true.
     */
    public void setProposingDraw(boolean proposingDraw) {
        isProposingDraw = proposingDraw;
    }



}