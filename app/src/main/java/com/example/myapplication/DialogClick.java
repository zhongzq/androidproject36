package com.example.myapplication;


import android.app.DialogFragment;


/**
 *
 * @author Zizhuo Tian & ZongQiZhong
 */

public interface DialogClick {

    void onDialogPositiveClick(DialogFragment dialog);
    void onDialogNegativeClick(DialogFragment dialog);

}
