package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.myapplication.R;


/**
 *
 * @author Zizhuo Tian & ZongQiZhong
 */

public class MainActivity extends BaseActivity implements View.OnClickListener{

    Button btnPlay;
    Button btnList;
    Button btnRelist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnPlay      = (Button) findViewById(R.id.btn_play);
        btnList     = (Button) findViewById(R.id.btn_list);
        btnRelist     = (Button) findViewById(R.id.btn_relist);
        btnPlay.setOnClickListener(this);
        btnList.setOnClickListener(this);
        btnRelist.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_play:
                Intent intent = new Intent(MainActivity.this, ChessActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_list:
                Intent intent1 = new Intent(MainActivity.this, GameListActivity.class);
                startActivity(intent1);
                break;
            case R.id.btn_relist:
                Intent intent2 = new Intent(MainActivity.this, HistoryActivity.class);
                intent2.putExtra(INTENT_DATA_KEY_FILENAME, INTENT_DATA_FILENAME_LAST);
                startActivity(intent2);
                break;
        }
    }
}
