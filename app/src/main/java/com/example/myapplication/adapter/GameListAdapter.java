package com.example.myapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.BaseActivity;
import com.example.myapplication.model.GameList;

/**
 * The class is a subclass of the BaseRecycleAdapter.
 * It used to make the data record representing as list.(Similar to list view's function in the JavaFX)
 * In out application, this part is related to showing the game history.
 *
 * @author Zizhuo Tian & ZongQiZhong
 */

public class GameListAdapter extends BaseRecyclerAdapter<GameList> {

    LayoutInflater inflater;
    Context mContext;

    public GameListAdapter(Context context) {
        this.mContext = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreate(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gamelist, parent, false);
        return new ViewHolder(layout);
    }

    @Override
    public void onBind(RecyclerView.ViewHolder viewHolder, final int RealPosition, final GameList data) {
        ((ViewHolder) viewHolder).title.setText(data.getTitle());
        ((ViewHolder) viewHolder).date.setText(data.getDate());
        ((ViewHolder) viewHolder).btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseActivity.deleteFile(mContext.getFilesDir(), data.getFileName());
                mDatas.remove(RealPosition);
                notifyDataSetChanged();
            }
        });
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView title,date;
        Button btn_delete;
        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.textview_title);
            date = (TextView) itemView.findViewById(R.id.textview_date);
            btn_delete = (Button) itemView.findViewById(R.id.btn_delete);
        }
    }
}
