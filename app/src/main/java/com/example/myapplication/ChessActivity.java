package com.example.myapplication;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.chess.AndroidChessController;
import com.example.myapplication.chess.Move;
import com.example.myapplication.model.Outcome;
import com.example.myapplication.model.Player;


/**
 *
 * @author Zizhuo Tian & ZongQiZhong
 */

public class ChessActivity extends ChessBoardActivity implements View.OnClickListener, DialogClick, DialogInterface.OnClickListener {
    //message.setText(input);
    //
    CharSequence pieces[] = new CharSequence[] {"Queen", "Rook", "Bishop", "Knight"};
    //
    ImageView img;
    TextView textviewMessage,textView_player;
    CheckBox checkboxDraw;
    Button btnAi;
    Button btnBack;
    Button btnDraw;
    Button btnResign;
    Button btnMain;
    Button btnNew;
    Button btnSave;
    Button btnReplay;

    //getText from a EditText in a DialogFragment
    private DialogTitle diaglogGetName;
    //
    //Dialog to pick a piece to promote to
    private AlertDialog.Builder piecePickerBuilder;

    //Controller
    AndroidChessController controller;


    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        DialogTitle diag = (DialogTitle)dialog;
        String szTemp = diag.getGameTitle();
        //
        szTemp = szTemp==null ? "" : szTemp;
        //
        if (szTemp!=null) {
            szTemp = szTemp.trim();
            if (szTemp.length()==0) {
                szTemp = "Not_Set";
            }
            //
            String out = serializeAddress(getFilesDir(), szTemp, game);
        }
    };


    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //
        setContentView(R.layout.activity_chess);
        //
        player      = (ImageView) findViewById(R.id.img);
        message     = (TextView) findViewById(R.id.textview_message);
        checkDraw   = (CheckBox) findViewById(R.id.checkbox_Draw);
        text_player = (TextView) findViewById(R.id.textview_player);
        btnBack = (Button) findViewById(R.id.btn_back);
        btnAi       = (Button) findViewById(R.id.btn_ai);
        btnResign   = (Button) findViewById(R.id.btn_resign);
        btnSave     = (Button) findViewById(R.id.btn_save);
        btnNew      = (Button) findViewById(R.id.btn_new);
        btnMain     = (Button) findViewById(R.id.btn_main);
        btnDraw     = (Button) findViewById(R.id.btn_draw);
//        btnReplay = (Button) findViewById(R.id.btn_replay);
        //
        btnBack.setOnClickListener(this);
        btnAi.setOnClickListener(this);
        btnResign.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        btnNew.setOnClickListener(this);
        btnMain.setOnClickListener(this);
        btnDraw.setOnClickListener(this);
//        btnReplay.setOnClickListener(this);
        //
        diaglogGetName = new DialogTitle();
        piecePickerBuilder = new AlertDialog.Builder(this);
        //
        controller = new AndroidChessController();
        doInitChessBoard(this, btnDraw);
        btnSave.setEnabled(false);
        btnSave.setTextColor(getResources().getColor(R.color.gray));
//        btnReplay.setEnabled(false);
    }


    @Override
    public void onClick(View v) {
        if (v instanceof ImageView) {
            if (controller.gameEnded()) {
                //do nothibng
            }
            else {
                ImageView iv = (ImageView)v;
                String szTag = (String)iv.getTag();
                int file            = szTag.charAt(0)-'0';
                int rank            = szTag.charAt(1)-'0';
                //
                String tagSelected = getSelected();
                //
                if (tagSelected!=null) {
                    if (isSelected(szTag)) {
                        doDeselectPiece();
                    }
                    else {
                        int fileSelected    = tagSelected.charAt(0)-'0';
                        int rankSelected    = tagSelected.charAt(1)-'0';
                        char pieceSelected  = tagSelected.charAt(3);
                        //
                        boolean isProposingDraw = checkDraw.isChecked();
                        //
                        Move move = new Move(file, rank, fileSelected, rankSelected, isProposingDraw);
                        doMove(move, pieceSelected=='P' && ((rank==0) || (rank==7)) );
                    }
                }
                else {
                    char colorPlayer    = szTag.charAt(2);
                    //
                    Player currentPlayer = controller.getCurrentPlayer();
                    if (colorPlayer=='W' && currentPlayer.isWhitePlayer()) {
                        doSelectPiece(szTag);
                    }
                    else if (colorPlayer=='B' && !currentPlayer.isWhitePlayer()) {
                        doSelectPiece(szTag);
                    }
                }
            }
        }
        else if (v instanceof Button) {
            if (!controller.gameEnded() && v==btnBack) {
                Move move = new Move("Rollback");
                doMove(move, false);
            }
            else if (!controller.gameEnded() && v==btnAi) {
                Move move = new Move("AI");
                doMove(move, false);
            }
            else if (!controller.gameEnded() && v==btnDraw) {
                //Move move = new Move("draw");
                //doMove(move, false);
                Player currentPlayer = controller.getCurrentPlayer();

                AlertDialog.Builder inputDialog = new AlertDialog.Builder(this);
                if(currentPlayer.isWhitePlayer()){
                    inputDialog.setTitle("Does the Black Square agree？");
                }
                else {
                    inputDialog.setTitle("Does the White square agree？");
                }

                //
                LayoutInflater inflater = getLayoutInflater();
                View view = inflater.inflate(R.layout.dialog_title, null);
                EditText gameTitle = (EditText) view.findViewById(R.id.gameTitle);
                gameTitle.setVisibility(View.GONE);
                //
                inputDialog.setView(view)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                Move move = new Move("resign");
                                doMove(move, false);
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
                inputDialog.show();
            }
            else if (!controller.gameEnded() && v==btnResign) {
                Move move = new Move("resign");
                doMove(move, false);
            }
            else if (v==btnSave) {
                diaglogGetName.show(getFragmentManager(), "Get Game Title");
            }
            else if (v==btnNew) {
                message.setText("New");
                //
                controller = new AndroidChessController();
                doCleanupChessBoard(btnDraw);
                btnSave.setEnabled(false);
                btnSave.setTextColor(getResources().getColor(R.color.gray));
//                btnReplay.setEnabled(false);
            }
            else if (v==btnMain) {
                Intent intent = new Intent(ChessActivity.this, MainActivity.class);
                startActivity(intent);
            }
            else if (v==btnReplay) {
                Intent intent = new Intent(ChessActivity.this, HistoryActivity.class);
                intent.putExtra(INTENT_DATA_KEY_FILENAME, INTENT_DATA_FILENAME_LAST);
                startActivity(intent);
            }
            else {
               //"draw"
            }
        }
    }


    @Override
    public void onClick(DialogInterface dialog, int which) {
        Move.Promotion promotion = Move.Promotion.NONE;
        if (which==0) {
            promotion = Move.Promotion.Queen;
        }
        else if (which==1) {
            promotion = Move.Promotion.Rook;
        }
        else if (which==2) {
            promotion = Move.Promotion.Bishop;
        }
        else if (which==3) {
            promotion = Move.Promotion.Knight;
        }
        else {
            promotion = Move.Promotion.Rook;
        }
        //
        mmove.setPromotion(promotion);
        //
        _doMove(mmove);
    }


    Move mmove;
    private void doMove(Move move, boolean isPromotion) {
        if (isPromotion) {
            mmove = move;
            piecePickerBuilder.setTitle("Promote the pawn to:");
            piecePickerBuilder.setItems(pieces, this);
            piecePickerBuilder.show();
        }
        else {
            _doMove(move);
        }
    }
    private void _doMove(Move move) {
        Outcome outcome = controller.doMove(move);
        //
        if (outcome.isOK()) {
            if (outcome.getGuiInstruction()!=null) {
                goGUI(outcome.getGuiInstruction());
                //
                Player currentPlayer = controller.getCurrentPlayer();
                setPlayer(currentPlayer.isWhitePlayer(), outcome.isProposingDraw(), btnDraw);
            }
            //
            message.setText(outcome.getReason());
            //
            if (controller.gameEnded()) {
                game = controller.game;
                controller.game = null;
                //
                diaglogGetName.show(getFragmentManager(), "Get Game Title");
                //
                btnSave.setEnabled(true);
                btnSave.setTextColor(getResources().getColor(R.color.white));
//                btnReplay.setEnabled(true);
            }
        }
        else {
            message.setText(outcome.getReason());
        }
    }
}
