package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.chess.Game;
import com.example.myapplication.chess.GameInstruction;


/**
 *
 * @author Zizhuo Tian & ZongQiZhong
 */

public class HistoryActivity extends ChessBoardActivity implements View.OnClickListener {
    //message.setText(input);
    Button btnNext;
    Button btnBack;
    Button btnPlay;
    Button btnMain;

    public static Game gameForPlayBack = null;
    int index;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        player          = (ImageView) findViewById(R.id.img);
        message         = (TextView) findViewById(R.id.textview_message);
        checkDraw       = (CheckBox) findViewById(R.id.checkbox_Draw);
        text_player = (TextView) findViewById(R.id.textview_player);

        checkDraw.setVisibility(View.INVISIBLE);

        btnNext      = (Button) findViewById(R.id.btn_next);
        btnMain     = (Button) findViewById(R.id.btn_main);
        btnPlay         = (Button) findViewById(R.id.btn_play);
        btnBack         = (Button) findViewById(R.id.btn_back);

        btnNext.setOnClickListener(this);
        btnMain.setOnClickListener(this);
        btnPlay.setOnClickListener(this);
        btnBack.setOnClickListener(this);

        doInitChessBoard(null, null);

        Intent intent = getIntent();
        String fileName = intent.getExtras().getString(INTENT_DATA_KEY_FILENAME);
        if (fileName!=null) {
            if (fileName.equalsIgnoreCase(INTENT_DATA_FILENAME_LAST)) {
                if (game !=null) {
                    gameForPlayBack = game;
                    message.setText(INTENT_DATA_FILENAME_LAST);
                }
                else {
                    if (gameForPlayBack ==null) {
                        message.setText("No game to replay");
                    }
                }
            }
            else {
                Object obj = readObjectFromFile(getFilesDir(), fileName);
                if (obj!=null) {
                    gameForPlayBack = (Game) obj;
                    message.setText(getTitleFromFileName(fileName));
                }
                else {
                    if (gameForPlayBack ==null) {
                        message.setText("Could not load game to replay");
                    }
                }
            }
        }
        else {
            if (game !=null) {
                gameForPlayBack = game;
                message.setText(INTENT_DATA_FILENAME_LAST);
            }
            else {
                if (gameForPlayBack ==null) {
                    message.setText("No game to replay");
                }
            }
        }
        //
        //
        index = 0;                  //next move forward
    }


    @Override
    public void onClick(View v) {
        if (v instanceof Button) {
            if (v==btnNext) {
                if (gameForPlayBack !=null && index>=0 && index< gameForPlayBack.getMoveCount()) {
                    GameInstruction guiInst = gameForPlayBack.getAt(index);
                    index++;
                    //
                    goGUI(guiInst.getMove());
                    //
                    setPlayer(!guiInst.isWhite(), false, null);
                    message.setText(guiInst.getMessage());
                }
            }
            else if (v==btnBack) {
                if (gameForPlayBack !=null && index>=1 && index<= gameForPlayBack.getMoveCount()) {
                    index--;
                    GameInstruction guiInst = gameForPlayBack.getAt(index);
                    //
                    goGUI(guiInst.getRollback());
                    //
                    if (index==0) {
                        setPlayer(true, false, null);
                        message.setText("");
                    }
                    else {
                        GameInstruction guiInstBefore = gameForPlayBack.getAt(index-1);;
                        if (guiInstBefore!=null) {
                            setPlayer(!guiInstBefore.isWhite(), false, null);
                            message.setText(guiInstBefore.getMessage());
                        }
                    }
                }
            }
            else if (v==btnPlay) {
                Intent intent = new Intent(HistoryActivity.this, ChessActivity.class);
                startActivity(intent);
            }
            else if (v==btnMain) {
                Intent intent = new Intent(HistoryActivity.this, MainActivity.class);
                startActivity(intent);
            }
            else {

            }
        }
    }
}
