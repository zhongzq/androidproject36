package com.example.myapplication.chess;

import com.example.myapplication.model.Chessboard;
import com.example.myapplication.model.Outcome;
import com.example.myapplication.model.Player;

import java.util.ArrayList;
import java.util.List;


/**
 * This class can be regard as a Chess Game Controller for Android System
 *
 * @author Zizhuo Tian & ZongQiZhong
 */

public class AndroidChessController {

    public Chessboard chessboard;
    public List<Player> players;
    public Game game;
    public boolean isWhitesTurn;
    public boolean isDrawProposed;
    public boolean isCheck;
    public String szResult;


    public AndroidChessController() {
        chessboard = new Chessboard();
        players = new ArrayList<>();
        players.add(new Player(true));
        players.add(new Player(false));
        //
        game = new Game();
        //
        isWhitesTurn    = true;
        isDrawProposed  = false;
        isCheck         = false;
        //
        szResult        = null;
    }


    /**
     * @param parameter Input file.
     */
    public void run(String parameter) {
        boolean isCheck = false;
        boolean isDrawProposed = false;
        while (true) {
            String input;
            //
            {
                if (isCheck) {
                    System.out.println("Check");
                    System.out.println();
                }
                System.out.print(getCurrentPlayer().getPrompt());
                System.out.println();
            }
            //
            Move move = new Move("");
            if (move.isValid()) {
                if (move.isDraw()) {
                    if (isDrawProposed) {
                        System.out.println("Draw");
                        break;
                    }
                    System.out.println("Illegal move, try again");
                    System.out.println();
                }
                else if (move.isResign()) {
                    System.out.println(!getCurrentPlayer().isWhitePlayer() ? "White wins" : "Black wins");
                    break;
                }
                else {
                    Outcome outcome = chessboard.doMove(move, getCurrentPlayer().isWhitePlayer());
                    //
                    if (outcome.isOK()) {
                        System.out.println();
                        //
                        if (outcome.isOpponentCheckMate()) {
                            System.out.println("Checkmate");
                            System.out.println();
                            System.out.println(getCurrentPlayer().isWhitePlayer() ? "White wins" : "Black wins");
                            break;
                        }
                        if (outcome.isOpponentStaleMate()) {
                            System.out.println("Stalemate");
                            System.out.println();
                            System.out.println("Draw");
                            break;
                        }
                        else {
                            switchPlayer();
                            //
                            isDrawProposed = move.isAskingDraw();
                            //
                            isCheck = outcome.isOpponentInCheck();
                        }
                    }
                    else {
                        System.out.println("Illegal move, try again");
                        System.out.println();
                    }
                }
            }
            else {
                System.out.println("Illegal move, try again");
                System.out.println();
            }
        }
    }


    public Outcome doMove(Move move) {
        Outcome outcome;
        if (move.isValid()) {
            if (move.isDraw()) {
                if (isDrawProposed) {
                    szResult = "Draw";
                    outcome = new Outcome(true, szResult);
                    //
                    GameInstruction guiInst = new GameInstruction("NOMOVE", "NOMOVE");
                    guiInst.setMessage(szResult);
                    game.addOne(guiInst);
                    //
                    outcome.setReason(szResult);
                }
                else {
                    outcome = new Outcome(false, "Illegal move, try again");
                }
            }
            else if (move.isResign()) {
                szResult = !getCurrentPlayer().isWhitePlayer() ? "White wins" : "Black wins";
                outcome = new Outcome(true, szResult);
                //
                GameInstruction guiInst = new GameInstruction("NOMOVE", "NOMOVE");
                guiInst.setMessage(szResult);
                game.addOne(guiInst);
                //
                outcome.setReason(szResult);
            }
            else {
                outcome = chessboard.doMove(move, getCurrentPlayer().isWhitePlayer());
                //
                if (outcome.isOK()) {
                    if (move.isRollback()) {
                        GameInstruction gi = game.rollbackOne();
                        //
                        outcome.setGuiInstruction(gi.getRollback());
                        //
                        switchPlayer();
                    }
                    else {
                        LastMoveForback lastMove = chessboard.getLastMoveForback();
                        //
                        GameInstruction gi = lastMove.getGUIInstruction();
                        gi.setWhite(getCurrentPlayer().isWhitePlayer());
                        //
                        game.addOne(gi);
                        //
                        outcome.setGuiInstruction(gi.getMove());
                        //
                        if (outcome.isOpponentCheckMate()) {
                            szResult = getCurrentPlayer().isWhitePlayer() ? "White wins" : "Black wins";
                            outcome.setReason(szResult);
                        }
                        if (outcome.isOpponentStaleMate()) {
                            szResult = "Draw";
                            outcome.setReason(szResult);
                        }
                        else {
                            switchPlayer();
                            //
                            isDrawProposed = move.isAskingDraw();
                            outcome.setProposingDraw(isDrawProposed);
                            //
                            isCheck = outcome.isOpponentInCheck();
                            if (isCheck) {
                                outcome.setReason("Check");
                            }
                        }
                        //
                        gi.setMessage(outcome.getReason());
                    }
                }
                else {
                    outcome = new Outcome(false, "Illegal move, try again");
                }
            }
        }
        else {
            outcome = new Outcome(false, "Illegal move, try again");
        }
        //
        return outcome;
    }



    /**
     * Checks for the current turn is white or black
     */
    public void switchPlayer() {
        isWhitesTurn = !isWhitesTurn;
    }


    /**
     * @return The current player.
     */
    public Player getCurrentPlayer() {
        return players.get(isWhitesTurn ? 0 : 1);
    }


    /**
     * @return  indicate the game ended and its result
     */
    public boolean gameEnded() {
        return szResult!=null;
    }















}