package com.example.myapplication.chess;


import com.example.myapplication.model.Position;

/**
 *
 * @author Zizhuo Tian & ZongQiZhong
 */

public class Target {
	Position sourceIndex;
	Position targetIndex;

	
	private Target() {
		sourceIndex = null;
		targetIndex	= null;
	}
	
	
	public Target(Position source, Position target) {
		sourceIndex = new Position(source);
		targetIndex	= new Position(target);
	}
	

	public Position getSource() {
		return sourceIndex;
	}

	
	public Position getTarget() {
		return targetIndex;
	}
}
