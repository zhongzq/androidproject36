package com.example.myapplication.chess;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * Implementation of saving and even replaying game.
 *
 * @author Zizhuo Tian & ZongQiZhong
 */

public class Game implements Serializable {
    public List<GameInstruction> moves;


    public Game() {
        moves = new ArrayList<>();
    }


    public void addOne(GameInstruction input) {
        moves.add(input);
    }


    public GameInstruction rollbackOne() {
        if (moves.size()>=1) {
            return moves.remove(moves.size() - 1);
        }
        else {
            return null;
        }
    }

    public GameInstruction getAt(int index) {
        if (index>=0 && index<=moves.size()-1) {
            return moves.get(index);
        }
        else {
            return null;
        }
    }

    public int getMoveCount() {
        return moves.size();
    }
}
