package com.example.myapplication.chess;

/**
 * Interface for TriPredicate
 *
 * @author Zizhuo Tian & ZongQiZhong
 */

public interface TriPredicate<T, U, V> {

	/**
	 * @param t Parameter
	 * @param u Parameter
	 * @param v Parameter
	 * @return True if TriPredicate criteria is met, false otherwise.
	 */
	boolean test(T t, U u, V v);
}