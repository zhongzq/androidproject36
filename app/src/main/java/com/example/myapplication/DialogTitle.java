package com.example.myapplication;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.example.myapplication.R;


/**
 *
 * @author Zizhuo Tian & ZongQiZhong
 */

public class DialogTitle extends DialogFragment {
    private DialogClick dialogClick;
    private EditText gameTitle;


    public String getGameTitle() {
        if (gameTitle==null) {
            return "Unknown";
        }
        return gameTitle.getText().toString();
    }

    @SuppressLint("InflateParams")
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder inputDialog = new AlertDialog.Builder(getActivity());
        //
        inputDialog.setTitle("Please enter title and hit OK if want To save the game, Or Cancel if not:");
        //
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_title, null);
        gameTitle = (EditText) view.findViewById(R.id.gameTitle);
        //
        inputDialog.setView(view)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialogClick.onDialogPositiveClick(DialogTitle.this);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialogClick.onDialogNegativeClick(DialogTitle.this);
                    }
                });
        //
        return inputDialog.create();
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        dialogClick = (DialogClick) activity;
    }
}