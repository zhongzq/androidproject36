package com.example.myapplication.piece;

import com.example.myapplication.chess.LastMoveForback;
import com.example.myapplication.chess.Move.*;
import com.example.myapplication.chess.Target;
import com.example.myapplication.model.Position;
import com.example.myapplication.model.Chessboard;
import com.example.myapplication.model.DirectionDistance;
import com.example.myapplication.model.Outcome;

import java.util.Map;


/**
 *
 * @author Zizhuo Tian & ZongQiZhong
 */
public abstract class Piece {


    private Position position = null;

    private boolean isWhite = false;

    private boolean isMoved = false;

    protected Chessboard chessBoard = null;


    /**
     * @param _isWhite True if piece is whiteIdentifying the color of the piece
     * @param file File index
     * @param rank Rank index
     * @param cb Chessboard
     */
    protected Piece(boolean _isWhite, int file, int rank, Chessboard cb) {
        position = new Position(file, rank);
	    isWhite = _isWhite;
	    chessBoard = cb;
    }

    /**
     * @param _isWhite Identifying the color of the piece
     * @param fileRank Input File and Rank
     * @param cb Chessboard
     */
    protected Piece(boolean _isWhite, String fileRank, Chessboard cb) {
        char charFile = fileRank.toUpperCase().trim().charAt(0);
        char charRank = fileRank.toUpperCase().trim().charAt(1);
        //
        position = new Position(charFile-'A', charRank - '1');
	    isWhite = _isWhite;
	    chessBoard = cb;
    }

    /**
     * @return Key for the map of pieces.
     */
    public String getKey() {
        return "" + position.fileIdx + "-" + position.rankIdx;
    }


    /**
     * @return True if piece is previously moved.
     */
    public boolean isMoved() {
        return isMoved;
    }

    
    /**
     * @return True if the piece is white.
     */
    public boolean isWhite() {
        return isWhite;
    }


    /**
     * @return Position of a piece.
     */
    public Position getPosition() {
        return position;
    }


    /**
     * @param input Position of a piece
     */
    protected void setPosition(Position input) {
        position = input;
    }


    /**
     * @param targetIndex Target indicating the target position of pieces
     * @param promotion Promotion. Pawn only.
     * @return Result of the move
     */
    abstract public Outcome doMove(Position targetIndex, Promotion promotion);


    /**
     * @param targetIndex Target indicating the target position of pieces
     * @return Result show the target is possible be attacked
     */
    abstract public Outcome doAttack(Position targetIndex);


    /**
     * @return True if it is a legal move
     */
    abstract public boolean hasLegalMove();

    /**
     * @return True if it is a legal move
     */
    abstract public Target getOneLegalMove();

    /**
     * @param targetIndex Target
     * @param rollback True for rollback after checking move
     * @return True if piece can move. False if move is illegal as King is in check after move.
     */
    protected boolean doActualMove(Position targetIndex, boolean rollback) {
        Map<String, Piece> pieceMap = chessBoard.getPieceMap();
        Position sourceIndex = getPosition();
    	Piece target = pieceMap.get(targetIndex.getKey());

        pieceMap.remove(getKey());
        setPosition(targetIndex);
        pieceMap.put(getKey(), this);

        Piece king =  isWhite() ? chessBoard.getWhiteKing() : chessBoard.getBlackKing();
        boolean isKingUnderAttack = chessBoard.isUnderAttack(isWhite(), king.getPosition());
        if (isKingUnderAttack || rollback) {
            pieceMap.remove(getKey());
            setPosition(sourceIndex);
            pieceMap.put(getKey(), this);
            if (target!=null) {
                pieceMap.put(target.getKey(), target);
            }
        }
        else {
            LastMoveForback lastMove = chessBoard.getLastMoveForback();
            lastMove.doInit(false);
            if (this instanceof Pawn) {
            	int dFile = targetIndex.fileIdx - sourceIndex.fileIdx;
            	int dRank = targetIndex.rankIdx - sourceIndex.rankIdx;
            	if ((dFile == 0) && Math.abs(dRank) == 2) {
            		lastMove.lastTwoStepPawnMove = this;
            	}
                else {
                	lastMove.lastTwoStepPawnMove = null;
                }
            }
            else {
            	lastMove.lastTwoStepPawnMove = null;
            }
            lastMove.isRegularMove = true;
            lastMove.sourceIndex = new Position(sourceIndex);
            lastMove.targetIndex = new Position(targetIndex);
            lastMove.isMove			= isMoved;						//before value
            lastMove.removedRegular = target;
            if (!isMoved) {
            	isMoved = true;
            }
        }
        return !isKingUnderAttack;
    }


    /**
     * @param targetIndex Target
     * @param rookSourceIndex Current position of rook
     * @param rookTargetIndex Target position of rook
     * @return True if King and Rook can castle. Always return true as King will not be under attack when castling begins.
     */
    protected boolean doActualMoveCastling(Position targetIndex, Position rookSourceIndex, Position rookTargetIndex) {
        Map<String, Piece> pieceMap = chessBoard.getPieceMap();
        //
        Position sourceIndex = getPosition();
        //
        pieceMap.remove(getKey());
        setPosition(targetIndex);
        pieceMap.put(getKey(), this);
        //
        Piece rook = pieceMap.remove(rookSourceIndex.getKey());
        rook.setPosition(rookTargetIndex);
        pieceMap.put(rook.getKey(), rook);
        //
        {	LastMoveForback lastMove = chessBoard.getLastMoveForback();
            lastMove.doInit(false);
            //
            lastMove.isCastling = true;
            //
            lastMove.sourceIndex 		= new Position(sourceIndex);
            lastMove.targetIndex 		= new Position(targetIndex);
            lastMove.sourceIndexRook 	= new Position(rookSourceIndex);
            lastMove.targetIndexRook	= new Position(rookTargetIndex);
        }
        //
        return true;
    }


    /**
     * @param targetIndex Target position of a move
     * @param enPassantIndex Index of pawn to be removed after enpassant.
     * @param rollback True if rollback is needed
     * @return True if pawn can en passant
     */
    protected boolean doActualMoveIsEnPassant(Position targetIndex, Position enPassantIndex, boolean rollback) {
        Map<String, Piece> pieceMap = chessBoard.getPieceMap();
    	//
        Position sourceIndex = getPosition();
        //
        pieceMap.remove(getKey());
        setPosition(targetIndex);
        pieceMap.put(getKey(), this);
        //
        Piece killedPaw = pieceMap.get(enPassantIndex.getKey());
        pieceMap.remove(enPassantIndex.getKey());
        //
        Piece king = isWhite() ? chessBoard.getWhiteKing() : chessBoard.getBlackKing();
        boolean isKingUnderAttack = chessBoard.isUnderAttack(isWhite(), king.getPosition());
        //
        if (isKingUnderAttack || rollback) {
            pieceMap.remove(getKey());
            setPosition(sourceIndex);
            pieceMap.put(getKey(), this);
            //
            pieceMap.put(killedPaw.getKey(), killedPaw);
        }
        else {
            LastMoveForback lastMove = chessBoard.getLastMoveForback();
            lastMove.doInit(false);
            //
            lastMove.isEnPassant = true;
            //
            lastMove.sourceIndex = new Position(sourceIndex);
            lastMove.targetIndex = new Position(targetIndex);
            //
            lastMove.isMove				= isMoved;						//before value
            lastMove.removedEnPassant 	= killedPaw;
        	//
        	if (!isMoved) {
        		isMoved = true;
        	}
        }
        //
        return !isKingUnderAttack;
    }


    /**
     * @param targetIndex Target
     * @param promotion Type of Promotion for pawn
     * @param rollback True if rollback is needed
     * @return True if promotion is successful
     */
    protected boolean doActualMoveIsPromotion(Position targetIndex, Promotion promotion/*Piece promPiece*/, boolean rollback) {
        Map<String, Piece> pieceMap = chessBoard.getPieceMap();
    	//
        Position sourceIndex = getPosition();
    	//
        Piece target = pieceMap.get(targetIndex.getKey());
    	//
        pieceMap.remove(getKey());
        //
        Piece promPiece;
        if (promotion == Promotion.Bishop) {
            promPiece = new Bishop(isWhite(), targetIndex.fileIdx, targetIndex.rankIdx, chessBoard);
        }
        else if (promotion == Promotion.Rook) {
            promPiece = new Rook(isWhite(), targetIndex.fileIdx, targetIndex.rankIdx, chessBoard);
        }
        else if (promotion == Promotion.Knight) {
            promPiece = new Knight(isWhite(), targetIndex.fileIdx, targetIndex.rankIdx, chessBoard);
        }
        else {
            promPiece = new Queen(isWhite(), targetIndex.fileIdx, targetIndex.rankIdx, chessBoard);
        }
        //pieceMap.put(promPiece.getKey(), promPiece);
        //
        Piece king = isWhite() ? chessBoard.getWhiteKing() : chessBoard.getBlackKing();
        boolean isKingUnderAttack = chessBoard.isUnderAttack(isWhite(), king.getPosition());
        //
        if (isKingUnderAttack || rollback) {
            pieceMap.put(getKey(), this);
            //
            if (target!=null) {
                pieceMap.put(target.getKey(), target);
            }
            else {
            	pieceMap.remove(targetIndex.getKey());
            }
        }
        else {
            LastMoveForback lastMove = chessBoard.getLastMoveForback();
            lastMove.doInit(false);
            lastMove.isPromotion = true;
            lastMove.sourceIndex = new Position(sourceIndex);
            lastMove.targetIndex = new Position(targetIndex);
            lastMove.removedRegular 		= target;
            lastMove.removedisPromotion 	= this;
            lastMove.promotionTo            = promPiece.toString();

        	if (!isMoved) {
        		isMoved = true;
        	}
        }

        return !isKingUnderAttack;
    }

    
    
    
    public static boolean doRollback(Chessboard chessBoard) {
    	LastMoveForback lastMove = chessBoard.getLastMoveForback();
        Map<String, Piece> pieceMap = chessBoard.getPieceMap();

        if (lastMove.isRegularMove) {
            Piece target = pieceMap.get(lastMove.targetIndex.getKey());

            pieceMap.remove(target.getKey());
            target.setPosition(lastMove.sourceIndex);
            pieceMap.put(target.getKey(), target);
            target.isMoved = lastMove.isMove;

            if (lastMove.removedRegular!=null) {
               	pieceMap.put(lastMove.removedRegular.getKey(), lastMove.removedRegular);
            }

            lastMove.doInit(true);

        	return true;
        }
        else if (lastMove.isCastling) {
            Piece target 		= pieceMap.get(lastMove.targetIndex.getKey());

            pieceMap.remove(target.getKey());
            target.setPosition(lastMove.sourceIndex);
            pieceMap.put(target.getKey(), target);
            Piece targetRook 	= pieceMap.get(lastMove.targetIndexRook.getKey());
            pieceMap.remove(targetRook.getKey());
            targetRook.setPosition(lastMove.sourceIndexRook);
            pieceMap.put(targetRook.getKey(), targetRook);
            lastMove.doInit(true);
        	return true;
        }
        else if (lastMove.isEnPassant) {
            Piece target = pieceMap.get(lastMove.targetIndex.getKey());
            pieceMap.remove(target.getKey());
            target.setPosition(lastMove.sourceIndex);
            pieceMap.put(target.getKey(), target);
            target.isMoved = lastMove.isMove;
           	pieceMap.put(lastMove.removedEnPassant.getKey(), lastMove.removedEnPassant);
            lastMove.doInit(true);
        	return true;
        }
        else if (lastMove.isPromotion) {
            Piece target 		= pieceMap.get(lastMove.targetIndex.getKey());
            pieceMap.remove(target.getKey());
           	pieceMap.put(lastMove.removedisPromotion.getKey(), lastMove.removedisPromotion);
            if (lastMove.removedRegular!=null) {
               	pieceMap.put(lastMove.removedRegular.getKey(), lastMove.removedRegular);
            }
            lastMove.doInit(true);
        	return true;
        }
        else {
        	return false;
        }
    }
    
    
    
    /**
     * @return Board output for white and black pieces.
     */
    @Override
    public String toString() {
        return isWhite ? "W" : "B";
    }


    /**
     * @param targetIndex Target
     * @return True if target position is empty
     */
    public boolean isTargetEmpty(Position targetIndex) {
        Piece targetPiece = chessBoard.getPieceMap().get(targetIndex.getKey());
        //
        // Target square has no piece
        return targetPiece == null;
    }

    /**
     * @param targetIndex Target
     * @return True True if the target position is opponent
     */
    public boolean isTargetEmptyOrLegal(Position targetIndex) {
        Piece targetPiece = chessBoard.getPieceMap().get(targetIndex.getKey());
        //
        // Target square has no piece or has piece of opposite color
        return (targetPiece == null) || (isWhite()==!targetPiece.isWhite());
    }



    /**
     * @param targetIndex Target
     * @return True if there is an en passant target
     */
    public boolean isTargetLegal(Position targetIndex) {
        Piece targetPiece = chessBoard.getPieceMap().get(targetIndex.getKey());
        //
        // Target square has no piece or has piece of opposite color
        return (targetPiece != null) && (isWhite()==!targetPiece.isWhite());
    }


    /**
     * @param targetIndex Target
     * @return True if there is an en passant target
     */
    public boolean isTargetLegalEnPassant(Position targetIndex) {
        LastMoveForback lastMove = chessBoard.getLastMoveForback();
        //
        Piece targetPiece = chessBoard.getPieceMap().get(targetIndex.getKey());
        //
        Piece LastTwoStepMovePaw = lastMove.getLastTwoStepMovePawn();
        //
        return (targetPiece != null) && targetPiece==LastTwoStepMovePaw;
    }


    /**
     * @param dd Direction and Distance
     * @return True if no pieces are between this piece and target
     */
    protected boolean isNoneInBetween(DirectionDistance dd) {
        Map<String, Piece> pieceMap = chessBoard.getPieceMap();
    	boolean out = true;
    	//
        for (int i = 1; i < dd.getDistance(); i++) {
        	int deltaFile = dd.getDeltaFile();
        	int deltaRank = dd.getDeltaRank();
            Position oneIndex = new Position(position.fileIdx + i * deltaFile, position.rankIdx + i * deltaRank);
            //
            Piece onePiece = pieceMap.get(oneIndex.getKey());
            if (onePiece!=null) {
            	out = false;
                break;
            }
        }
    	//
    	return out;
    }

    /**
     * @param deltaFile Change in file for each step
     * @param deltaRank Change in rank for each step
     * @return True if there is at least 1 legal move along 1 direction
     */
    protected Target hasLegalMoveOneDirection(int deltaFile, int deltaRank) {
    	Position sourceIndex = getPosition();
    	//
        Map<String, Piece> pieceMap = chessBoard.getPieceMap();
        Target legalMove = null;
		//
    	for (int file = sourceIndex.fileIdx + deltaFile, rank = sourceIndex.rankIdx + deltaRank; (file >= 0 && file < 8 && rank >= 0 && rank < 8); file = file + deltaFile, rank = rank + deltaRank) {
    		Position targetIndex = new Position(file, rank);
    		//
            boolean isLegalMove;
            Piece targetPiece = pieceMap.get(targetIndex.getKey());
            if (targetPiece != null && targetPiece.isWhite() == isWhite()) {			//piece same color
                break;
            }
            else if ((targetPiece != null && targetPiece.isWhite() != isWhite())) {		//piece different color
                isLegalMove = doActualMove(targetIndex, true);
                if (!isLegalMove) {
                    break;
                }
            }
            else {
                isLegalMove = doActualMove(targetIndex, true);
            }
            //
            if (isLegalMove) {
            	legalMove = new Target(sourceIndex, targetIndex);
                break;
            }
        }
    	//
    	return legalMove;
    }


    /**
     * @param deltaFile Change in file for each step
     * @param deltaRank Change in rank for each step
     * @return True if is legal for 1 step in 1 direction
     */
    protected Target hasLegalMoveOneDirectionOneStep(int deltaFile, int deltaRank) {
    	Position sourceIndex = getPosition();
    	//
        Map<String, Piece> pieceMap = chessBoard.getPieceMap();
        Target legalMove = null;
		//
    	for (int file=sourceIndex.fileIdx+deltaFile, rank=sourceIndex.rankIdx+deltaRank; (file>=0 && file<8 && rank>=0 && rank<8); file=100, rank=-100) {
    		Position targetIndex = new Position(file, rank);
    		//
    		Piece targetPiece = pieceMap.get(targetIndex.getKey());
    		if (targetPiece != null && targetPiece.isWhite() == isWhite()) {
    			break;
    		}
            boolean isLegalMove = doActualMove(targetIndex, true);
            //
            if (isLegalMove) {
            	legalMove = new Target(sourceIndex, targetIndex);;
                break;
            }
        }
    	//
    	return legalMove;
    }
}