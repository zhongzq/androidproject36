package com.example.myapplication.piece;

import com.example.myapplication.chess.Target;
import com.example.myapplication.model.Position;
import com.example.myapplication.model.Chessboard;
import com.example.myapplication.model.DirectionDistance;
import com.example.myapplication.model.Outcome;
import com.example.myapplication.chess.Move.*;

/**
 *This class create a chess piece - Pawn and defines the initial position and valid moves of pawn.
 *
 * @author Zizhuo Tian & ZongQiZhong
 *
 */

public class Pawn extends Piece {

    /**
     * @param isWhite Identifying the color of the piece
     * @param file File index
     * @param rank Rank index
     * @param cb Chessboard
     */
    public Pawn(boolean isWhite, int file, int rank, Chessboard cb) {
        super(isWhite, file, rank, cb);
        cb.getPieceMap().put(getKey(), this);
    }

    /**
     * @param isWhite  Identifying the color of the piece
     * @param fileRank Input File and Rank
     * @param cb Chessboard
     */
    public Pawn(boolean isWhite, String fileRank, Chessboard cb) {
        super(isWhite, fileRank, cb);
        cb.getPieceMap().put(getKey(), this);
    }

	/**
	 * @return The board output for Pawn.
	 */
	@Override
	public String toString() {
		return super.toString() + "p";
	}

    /**
     * @param targetIndex Target indicating the target position of pieces
     * @param promotion Promotion.
     * @return Result of the move.
     */
    @Override
    public Outcome doMove(Position targetIndex, Promotion promotion) {
        return doMoveInternal(targetIndex, true, false, promotion);
    }


    /**
     * @param targetIndex Target indicating the target position of pieces
     * @return Result show the target is possible be attacked
     */
    public Outcome doAttack(Position targetIndex) {
    	Promotion promotion;
    	promotion = Promotion.NONE;
        return doMoveInternal(targetIndex, false, false, promotion);
    }


    /**
     * @param targetIndex Target indicating the target position of pieces
     * @param doMove True if piece perform move, false checks if piece can attack target.
     * @param rollback If true, rollback after checking move
     * @param promotion Promotion
     * @return Result of the move
     */
    private Outcome doMoveInternal(Position targetIndex, boolean doMove, boolean rollback, Promotion promotion) {
        Outcome outcome;
        //
        Position currentIndex = getPosition();
        //
        DirectionDistance dd = new DirectionDistance(currentIndex.fileIdx, currentIndex.rankIdx, targetIndex.fileIdx, targetIndex.rankIdx);
        //
        if (doMove && dd.isPawOneStep(isWhite())) {
       		if (isTargetEmpty(targetIndex)) {
       			if (doMove) {
	                boolean isPromotion = dd.isPawnPromotion(isWhite());
	       			boolean isLegal;
	       			// Promotion
	       			if (isPromotion) {
	       				isLegal = doActualMoveIsPromotion(targetIndex, promotion, rollback);
	       			}
	       			else {
	       				isLegal = doActualMove(targetIndex, rollback);
	       			}
	       			//
                    outcome = isLegal ? new Outcome(true, "") : new Outcome(false, "Illegal move, try again");    // King will be in check
       			}
       			else {
           			outcome = new Outcome(true, "");
       			}
       		}
       		else {
       		    // Pawn illegal move one step
       			outcome = new Outcome(false, "Illegal move, try again");
       		}
        }
        else if (dd.isPawnKill(isWhite())) {
       		if (isTargetLegal(targetIndex)) {
       			if (doMove) {
	                boolean isPromotion = dd.isPawnPromotion(isWhite());
	       			boolean isLegal;
	       			//
	       			if (isPromotion) {
	       				isLegal = doActualMoveIsPromotion(targetIndex, promotion, rollback);
	       			}
	       			else {
	       				isLegal = doActualMove(targetIndex, rollback);
	       			}
	       			//
                    outcome = isLegal ? new Outcome(true, "") : new Outcome(false, "Illegal move, try again");     // King will be in check
       			}
       			else {
           			outcome = new Outcome(true, "OK");
       			}
       		}
       		// En passant
            else if (isTargetEmpty(targetIndex) && dd.isEnPassant(isWhite())) {
       			Position enPassantIndex = new Position(targetIndex.fileIdx, currentIndex.rankIdx);
       			if (isTargetLegalEnPassant(enPassantIndex)) {
       				if (doMove) {
       					boolean isLegal = doActualMoveIsEnPassant(targetIndex, enPassantIndex, rollback);
       					if (isLegal) {
       						outcome = new Outcome(true, "OK");
       					}
       					else {
       					    // King will be in check
       						outcome = new Outcome(false, "Illegal move, try again");
       					}
       				}
       				else {
       					outcome = new Outcome(true, "OK");
       				}
       			}
       			else {
       			    // Pawn En passant target illegal
       				outcome = new Outcome(false, "Illegal move, try again");
       			}
            }
       		else {
       		    // Pawn target illegal
       			outcome = new Outcome(false, "Illegal move, try again");
       		}
        }
        else if (doMove && dd.isPawnTwoStep(isWhite())) {
       		if (isTargetEmpty(targetIndex) && isTargetEmpty(new Position(currentIndex.fileIdx, (currentIndex.rankIdx+targetIndex.rankIdx)/2))) {
       			if (doMove) {
	       			boolean isLegal = doActualMove(targetIndex, rollback);
	       			if (isLegal) {
	           			outcome = new Outcome(true, "OK");
	       			}
	       			else {
                        // King will be in check
                        outcome = new Outcome(false, "Illegal move, try again.");
	       			}
       			}
       			else {
           			outcome = new Outcome(true, "OK");
       			}
       		}
       		else {
       		    // A piece is in the way
       			outcome = new Outcome(false, "Illegal move, try again");
       		}
        }
        else {
            outcome = new Outcome(false, "Illegal move, try again");
        }
        //
        return outcome;
    }


    /**
     * @return True if Pawn has legal move.
     */
    @Override
	public boolean hasLegalMove() {
		if (isWhite()) {
			return 		isLegalMove(0, 1)!=null
					|| 	isLegalMove(0, 2)!=null 
					|| 	isLegalMove(1, 1)!=null
					|| 	isLegalMove(-1, 1)!=null;
		}
        return 		isLegalMove(0, -1)!=null
        		|| 	isLegalMove(0, -2)!=null
        		|| 	isLegalMove(1, -1)!=null
        		|| 	isLegalMove(-1, -1)!=null;
    }

    
	
	
    @Override
    public Target getOneLegalMove() {
    	Target sat = null;
    	//
		if (isWhite()) {
	    	sat = isLegalMove(0, 1);
	    	if (sat!=null) {
	    		return sat;
	    	}
	    	//
	    	sat = isLegalMove(0, 2);
	    	if (sat!=null) {
	    		return sat;
	    	}
	    	//
	    	sat = isLegalMove(1, 1);
	    	if (sat!=null) {
	    		return sat;
	    	}
	    	//
	    	sat = isLegalMove(-1, 1);
	    	if (sat!=null) {
	    		return sat;
	    	}
	    	//
	    	return sat;
		}
		else {
	    	sat = isLegalMove(0, -1);
	    	if (sat!=null) {
	    		return sat;
	    	}
	    	//
	    	sat = isLegalMove(0, -2);
	    	if (sat!=null) {
	    		return sat;
	    	}
	    	//
	    	sat = isLegalMove(1, -1);
	    	if (sat!=null) {
	    		return sat;
	    	}
	    	//
	    	sat = isLegalMove(-1, -1);
	    	if (sat!=null) {
	    		return sat;
	    	}
	    	//
	    	return sat;
		}
    	
    	//
    }
    
    
    
    /**
     * @param deltaFile Change in file for each step.
     * @param deltaRank Change in rank for each step.
     * @return True if legal move after change, false if illegal move.
     */
    private Target isLegalMove(int deltaFile, int deltaRank) {
		Position currentIndex = getPosition();
		Target legalMove = null;
		//
		int file = currentIndex.fileIdx + deltaFile;
		int rank = currentIndex.rankIdx + deltaRank;
		//
		if (file >= 0 && file < 8 && rank >= 0 && rank < 8) {
    		Position targetIndex = new Position(file, rank);
    		//
        	Promotion promotion;
        	promotion = Promotion.NONE;
			Outcome outcome = doMoveInternal(targetIndex, true, true, promotion);
			//
			if (outcome.isOK()) {
				legalMove = new Target(currentIndex, targetIndex);
			}
			else {
				//do nothing
			};
		}
		//
		return legalMove;
	}
    
    

}