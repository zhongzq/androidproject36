package com.example.myapplication.piece;

import com.example.myapplication.chess.Target;
import com.example.myapplication.model.Position;
import com.example.myapplication.model.Chessboard;
import com.example.myapplication.chess.Move.*;
import com.example.myapplication.model.DirectionDistance;
import com.example.myapplication.model.Outcome;


/**
 *This class create a chess piece - Queen and defines the initial position and valid moves of queen.
 *
 * @author Zizhuo Tian & ZongQiZhong
 *
 */

public class Queen extends Piece {

    /**
     * @param isWhite Identifying the color of the piece
     * @param file File index
     * @param rank Rank index
     * @param cb Chessboard
     */
    public Queen(boolean isWhite, int file, int rank, Chessboard cb) {
        super(isWhite, file, rank, cb);
        cb.getPieceMap().put(getKey(), this);
    }

    /**
     * @param isWhite  Identifying the color of the piece
     * @param fileRank Input File and Rank
     * @param cb Chessboard
     */
    public Queen(boolean isWhite, String fileRank, Chessboard cb) {
        super(isWhite, fileRank, cb);
        cb.getPieceMap().put(getKey(), this);
    }


    /**
     * @return The board output for the Queen.
     */
    @Override
    public String toString() {
        return super.toString() + "Q";
    }


    /**
     * @param targetIndex Target indicating the target position of pieces
     * @param promotion Promotion.
     * @return Result of the move.
     */
    public Outcome doMove(Position targetIndex, Promotion promotion) {
        return doMoveInternal(targetIndex, true);
    }

    /**
     * @param targetIndex Target indicating the target position of pieces
     * @return  Result show the target is possible be attacked
     */
    public Outcome doAttack(Position targetIndex) {
        return doMoveInternal(targetIndex, false);
    }

    /**
     * @param targetIndex Target indicating the target position of pieces
     * @param doMove True if piece perform move. False checks if piece can attack target.
     * @return Result of the move.
     */
    private Outcome doMoveInternal(Position targetIndex, boolean doMove) {
        Outcome outcome;
        //
        Position currentIndex = getPosition();
        //
        DirectionDistance dd = new DirectionDistance(currentIndex.fileIdx, currentIndex.rankIdx, targetIndex.fileIdx, targetIndex.rankIdx);
        //
        if (dd.isValid()) {
            if (isTargetEmptyOrLegal(targetIndex)) {
                if (isNoneInBetween(dd)) {
                    if (doMove) {
                    	boolean isLegal = doActualMove(targetIndex, false);
                        if (isLegal) {
                            outcome = new Outcome(true, "OK");
                        }
                        else {
                            // King in check
                            outcome = new Outcome(false, "Illegal move, try again");
                        }
                    }
                    else {
                        outcome = new Outcome(true, "OK");
                    }
                }
                else {
                    // Piece(s) in between, try again
                    outcome = new Outcome(false, "Illegal move, try again");
                }
            }
            else {
                // Wrong target
                outcome = new Outcome(false, "Illegal move, try again");
            }
        }
        else {
            outcome = new Outcome(false, "Illegal move, try again");
        }
        //
        return outcome;
    }


    /**
     * @return True if Queen has legal move. For checkmate and stalemate.
     */
    @Override
    public boolean hasLegalMove() {
		return hasLegalMoveOneDirection(0, 1)!=null
                || hasLegalMoveOneDirection(0, -1)!=null
                || hasLegalMoveOneDirection(1, 0)!=null
                || hasLegalMoveOneDirection(-1, 0)!=null
                || hasLegalMoveOneDirection(1, 1)!=null
                || hasLegalMoveOneDirection(1, -1)!=null
                || hasLegalMoveOneDirection(-1, 1)!=null
                || hasLegalMoveOneDirection(-1, -1)!=null;
    }

    
	
	
    @Override
    public Target getOneLegalMove() {
    	Target sat = null;
    	//
    	sat = hasLegalMoveOneDirection(0, 1);
    	if (sat!=null) {
    		return sat;
    	}
    	//
    	sat = hasLegalMoveOneDirection(0, -1);
    	if (sat!=null) {
    		return sat;
    	}
    	//
    	sat = hasLegalMoveOneDirection(1, 0);
    	if (sat!=null) {
    		return sat;
    	}
    	//
    	sat = hasLegalMoveOneDirection(-1, 0);
    	if (sat!=null) {
    		return sat;
    	}
    	//
    	sat = hasLegalMoveOneDirection(1, 1);
    	if (sat!=null) {
    		return sat;
    	}
    	//
    	sat = hasLegalMoveOneDirection(1, -1);
    	if (sat!=null) {
    		return sat;
    	}
    	//
    	sat = hasLegalMoveOneDirection(-1, 1);
    	if (sat!=null) {
    		return sat;
    	}
    	//
    	sat = hasLegalMoveOneDirection(-1, -1);
    	if (sat!=null) {
    		return sat;
    	}
    	//
    	return sat;
    }
    
    


}
