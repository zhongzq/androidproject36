package com.example.myapplication.piece;

import com.example.myapplication.chess.Target;
import com.example.myapplication.model.Position;
import com.example.myapplication.model.Chessboard;
import com.example.myapplication.chess.Move.*;
import com.example.myapplication.model.DirectionDistance;
import com.example.myapplication.model.Outcome;


/**
 *This class create a chess piece - Rock and defines the initial position and valid moves of rock.
 *
 * @author Zizhuo Tian & ZongQiZhong
 *
 */

public class Rook extends Piece {
    
    /**
     * @param isWhite  Identifying the color of the piece
     * @param file File of current location.
     * @param rank Rank of current location.
     * @param cb Chessboard
     */
    public Rook(boolean isWhite, int file, int rank, Chessboard cb) {
        super(isWhite, file, rank, cb);
        cb.getPieceMap().put(getKey(), this);
    }

    /**
     * @param isWhite Identifying the color of the piece
     * @param fileRank Input File and Rank
     * @param cb Chessboard
     */
    public Rook(boolean isWhite, String fileRank, Chessboard cb) {
        super(isWhite, fileRank, cb);
        cb.getPieceMap().put(getKey(), this);
    }


    /**
     * @param targetIndex Target index a piece moves too.
     * @param promotion Promotion. Only valid for pawn.
     * @return Outcome of the move.
     */
    public Outcome doMove(Position targetIndex, Promotion promotion) {
        return doMoveInternal(targetIndex, true);
    }

    /**
     * @return The board output for the Rook.
     */
    @Override
    public String toString() {
        return super.toString() + "R";
    }


    /**
     * @param targetIndex  Target indicating the target position of pieces
     * @return Result show the target is possible be attacked
     */
    public Outcome doAttack(Position targetIndex) {
        return doMoveInternal(targetIndex, false);
    }

    /**
     * @param targetIndex Target indicating the target position of pieces
     * @param doMove True if piece perform move. False checks if piece can attack target.
     * @return Result of the move.
     */
    private Outcome doMoveInternal(Position targetIndex, boolean doMove) {
        Outcome outcome;
        //
        Position currentIndex = getPosition();
        //
        DirectionDistance dd = new DirectionDistance(currentIndex.fileIdx, currentIndex.rankIdx, targetIndex.fileIdx, targetIndex.rankIdx);
        //
        if (dd.isParallel()) {
            if (isTargetEmptyOrLegal(targetIndex)) {
                if (isNoneInBetween(dd)) {
                    if (doMove) {
                        boolean isLegal = doActualMove(targetIndex, false);
                        if (isLegal) {
                            outcome = new Outcome(true, "OK");
                        }
                        else {
                            // King in check, try again
                            outcome = new Outcome(false, "Illegal move, try again");
                        }
                    }
                    else {
                        outcome = new Outcome(true, "OK");
                    }
                }
                else {
                    // Piece(s) in between, try again
                    outcome = new Outcome(false, "Illegal move, try again");
                }
            }
            else {
                // Wrong target, try again
                outcome = new Outcome(false, "Illegal move, try again");
            }
        }
        else {
            outcome = new Outcome(false, "Illegal move, try again");
        }
        //
        return outcome;
    }


    /**
     * @return True if Rook has legal move. For checkmate and stalemate.
     */
    @Override
    public boolean hasLegalMove() {
		return hasLegalMoveOneDirection(0, 1)!=null
                || hasLegalMoveOneDirection(0, -1)!=null
                || hasLegalMoveOneDirection(1, 0)!=null
                || hasLegalMoveOneDirection(-1, 0)!=null;
    }
	
	
    @Override
    public Target getOneLegalMove() {
    	Target sat = null;
    	//
    	sat = hasLegalMoveOneDirection(0, 1);
    	if (sat!=null) {
    		return sat;
    	}
    	//
    	sat = hasLegalMoveOneDirection(0, -1);
    	if (sat!=null) {
    		return sat;
    	}
    	//
    	sat = hasLegalMoveOneDirection(1, 0);
    	if (sat!=null) {
    		return sat;
    	}
    	//
    	sat = hasLegalMoveOneDirection(-1, 0);
    	if (sat!=null) {
    		return sat;
    	}
    	//
    	return sat;
    }
    
    


}