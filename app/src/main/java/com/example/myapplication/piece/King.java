package com.example.myapplication.piece;

import com.example.myapplication.chess.Move;
import com.example.myapplication.chess.Target;
import com.example.myapplication.model.Position;
import com.example.myapplication.model.Chessboard;
import com.example.myapplication.model.DirectionDistance;
import com.example.myapplication.model.Outcome;

import java.util.Map;


/**
 *This class create a chess piece - King and defines the initial position and valid moves of king.
 *
 *@author Zizhuo Tian & ZongQiZhong
 *
 */

public class King extends Piece {

	/**
	 * @param isWhite Identifying the color of the piece
	 * @param file    File index
	 * @param rank    Rank index
	 * @param cb      Chessboard
	 */
	public King(boolean isWhite, int file, int rank, Chessboard cb) {
		super(isWhite, file, rank, cb);
		cb.getPieceMap().put(getKey(), this);
	}

	/**
	 * @param isWhite  Identifying the color of the piece
	 * @param fileRank Input of File and Rank
	 * @param cb       Chessboard
	 */
	public King(boolean isWhite, String fileRank, Chessboard cb) {
		super(isWhite, fileRank, cb);
		cb.getPieceMap().put(getKey(), this);
	}

	/**
	 * @return The board output for the King.
	 */
	@Override
	public String toString() {
		return super.toString() + "K";
	}


	/**
	 * @param targetIndex Target indicating the target position of pieces
	 * @param promotion   Promotion.
	 * @return Result of move
	 */
	@Override
	public Outcome doMove(Position targetIndex, Move.Promotion promotion) {
		return doMoveInternal(targetIndex, true);
	}

	/**
	 * @param targetIndex Target indicating the target position of pieces
	 * @return Result show the target is possible be attacked
	 */
	public Outcome doAttack(Position targetIndex) {
		return doMoveInternal(targetIndex, false);
	}

	/**
	 * @param targetIndex Target indicating the target position of pieces
	 * @param doMove      True if piece perform move, false checks if piece can attack target
	 * @return Result of the move
	 */
	private Outcome doMoveInternal(Position targetIndex, boolean doMove) {
		Outcome outcome;
		//
		Map<String, Piece> pieceMap = chessBoard.getPieceMap();
		//
		Position currentIndex = getPosition();
		//
		DirectionDistance dd = new DirectionDistance(currentIndex.fileIdx, currentIndex.rankIdx, targetIndex.fileIdx, targetIndex.rankIdx);
		//
		if (dd.isRegal()) {
			if (isTargetEmptyOrLegal(targetIndex)) {
				if (doMove) {
					boolean isLegal = doActualMove(targetIndex, false);
					if (isLegal) {
						outcome = new Outcome(true, "OK");
					} else {
						// King not safe
						outcome = new Outcome(false, "Illegal move, try again");
					}
				} else {
					outcome = new Outcome(true, "OK");
				}
			} else {
				outcome = new Outcome(false, "Illegal move, try again");
			}
		}

		else if (dd.isCastlingQS(isWhite()) || dd.isCastlingKS(isWhite())) {
			Position rookSourceIndex;
			Position rookTargetIndex;
			if (isWhite()) {
				if (dd.isCastlingQS(isWhite())) {
					rookSourceIndex = new Position(0, 0);
					rookTargetIndex = new Position(3, 0);
				} else {
					rookSourceIndex = new Position(7, 0);
					rookTargetIndex = new Position(5, 0);
				}
			} else if (dd.isCastlingQS(isWhite())) {
				rookSourceIndex = new Position(0, 7);
				rookTargetIndex = new Position(3, 7);
			} else {
				rookSourceIndex = new Position(7, 7);
				rookTargetIndex = new Position(5, 7);
			}
			Piece rook = pieceMap.get(rookSourceIndex.getKey());
			if (!isMoved() && (rook != null) && !rook.isMoved()) {
				DirectionDistance ddCastling = new DirectionDistance(currentIndex.fileIdx, currentIndex.rankIdx, rookSourceIndex.fileIdx, rookSourceIndex.rankIdx);
				if (isNoneInBetween(ddCastling)) {
					boolean isUnderAttack1 = chessBoard.isUnderAttack(isWhite(), currentIndex);
					boolean isUnderAttack2 = chessBoard.isUnderAttack(isWhite(), targetIndex);
					boolean isUnderAttack3 = chessBoard.isUnderAttack(isWhite(), new Position((currentIndex.fileIdx + targetIndex.fileIdx) / 2, currentIndex.rankIdx));
					if (!isUnderAttack1 && !isUnderAttack2 && !isUnderAttack3) {
						if (doMove) {
							boolean isLegal = doActualMoveCastling(targetIndex, rookSourceIndex, rookTargetIndex);
							//
							if (isLegal) {
								outcome = new Outcome(true, "OK");
							} else {
								// King not safe
								outcome = new Outcome(false, "Illegal move, try again");
							}
						} else {
							outcome = new Outcome(true, "OK");
						}
					} else {
						// Under attack
						outcome = new Outcome(false, "Illegal move, try again");
					}
				} else {
					// Pieces between king and rook
					outcome = new Outcome(false, "Illegal move, try again");
				}
			} else {
				// King or rook moved before
				outcome = new Outcome(false, "Illegal move, try again");
			}
		} else {
			return new Outcome(false, "Illegal move, try again");
		}
		//
		return outcome;
	}

	/**
	 * @return True if piece has legal move for checkmate and stalemate, false otherwise.
	 */
	@Override
	public boolean hasLegalMove() {
		// No need to check castling, since if castling is legal, move one step toward rook should be legal too.
		return hasLegalMoveOneDirectionOneStep(0, 1) != null
				|| hasLegalMoveOneDirectionOneStep(0, -1) != null
				|| hasLegalMoveOneDirectionOneStep(1, 0) != null
				|| hasLegalMoveOneDirectionOneStep(-1, 0) != null
				|| hasLegalMoveOneDirectionOneStep(1, 1) != null
				|| hasLegalMoveOneDirectionOneStep(1, -1) != null
				|| hasLegalMoveOneDirectionOneStep(-1, 1) != null
				|| hasLegalMoveOneDirectionOneStep(-1, -1) != null;
	}


	@Override
	public Target getOneLegalMove() {
		Target sat = null;
		//
		sat = hasLegalMoveOneDirectionOneStep(0, 1);
		if (sat != null) {
			return sat;
		}
		//
		sat = hasLegalMoveOneDirectionOneStep(0, -1);
		if (sat != null) {
			return sat;
		}
		//
		sat = hasLegalMoveOneDirectionOneStep(1, 0);
		if (sat != null) {
			return sat;
		}
		//
		sat = hasLegalMoveOneDirectionOneStep(-1, 0);
		if (sat != null) {
			return sat;
		}
		//
		sat = hasLegalMoveOneDirectionOneStep(1, 1);
		if (sat != null) {
			return sat;
		}
		//
		sat = hasLegalMoveOneDirectionOneStep(1, -1);
		if (sat != null) {
			return sat;
		}
		//
		sat = hasLegalMoveOneDirectionOneStep(-1, 1);
		if (sat != null) {
			return sat;
		}
		//
		sat = hasLegalMoveOneDirectionOneStep(-1, -1);
		if (sat != null) {
			return sat;
		}
		//
		return sat;
	}


}