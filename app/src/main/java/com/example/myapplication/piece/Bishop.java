package com.example.myapplication.piece;

import com.example.myapplication.chess.Move;
import com.example.myapplication.chess.Target;
import com.example.myapplication.model.Position;
import com.example.myapplication.model.Chessboard;
import com.example.myapplication.model.DirectionDistance;
import com.example.myapplication.model.Outcome;

/**
 *This class create a chess piece - Bishop and defines the initial position and valid moves of bishop.
 *
 *
 * @author Zizhuo Tian & ZongQiZhong
 *
 */

public class Bishop extends Piece {

    /**
     * @param isWhite Identifying the color of the piece
     * @param file    File index
     * @param rank    Rank index
     * @param cb      Chessboard
     */
    public Bishop(boolean isWhite, int file, int rank, Chessboard cb) {
        super(isWhite, file, rank, cb);
        cb.getPieceMap().put(getKey(), this);
    }


    /**
     * @param isWhite  Identifying the color of the piece
     * @param fileRank Input of File and Rank
     * @param cb       Chessboard
     */
    public Bishop(boolean isWhite, String fileRank, Chessboard cb) {
        super(isWhite, fileRank, cb);
        cb.getPieceMap().put(getKey(), this);
    }


    /**
     * @return The board output for Bishop.
     */
    @Override
    public String toString() {
        return super.toString() + "B";
    }


    /**
     * @param targetIndex Target indicating the target position of pieces
     * @param promotion   Promotion
     * @return Result of the move
     */
    public Outcome doMove(Position targetIndex, Move.Promotion promotion) {
        return doMoveInternal(targetIndex, true);
    }


    /**
     * @param targetIndex Target indicating the target position of pieces
     * @return Result showing the target is possible be attacked
     */
    public Outcome doAttack(Position targetIndex) {
        return doMoveInternal(targetIndex, false);
    }


    /**
     * @param targetIndex Target index a piece moves too.
     * @param doMove      True if piece perform move, false checks if piece can attack target.
     * @return Outcome of the move.
     */
    private Outcome doMoveInternal(Position targetIndex, boolean doMove) {
        Outcome outcome;
        //
        Position currentIndex = getPosition();
        //
        DirectionDistance dd = new DirectionDistance(currentIndex.fileIdx, currentIndex.rankIdx, targetIndex.fileIdx, targetIndex.rankIdx);
        //
        if (dd.isDiagonal()) {
            if (isTargetEmptyOrLegal(targetIndex)) {
                if (isNoneInBetween(dd)) {
                    if (doMove) {
                        boolean isLegal = doActualMove(targetIndex, false);
                        if (isLegal) {
                            outcome = new Outcome(true, "OK");
                        } else {
                            // King in check
                            outcome = new Outcome(false, "Illegal move, try again");
                        }
                    } else {
                        outcome = new Outcome(true, "OK");
                    }
                } else {
                    // Piece(s) in between, try again
                    outcome = new Outcome(false, "Illegal move, try again");
                }
            } else {
                // Target piece is the same color
                outcome = new Outcome(false, "Illegal move, try again");
            }
        } else {
            outcome = new Outcome(false, "Illegal move");
        }
        //
        return outcome;
    }


    /**
     * @return True if Bishop has legal move. For checkmate and stalemate.
     */
    @Override
    public boolean hasLegalMove() {
        return hasLegalMoveOneDirection(1, 1) != null
                || hasLegalMoveOneDirection(1, -1) != null
                || hasLegalMoveOneDirection(-1, 1) != null
                || hasLegalMoveOneDirection(-1, -1) != null;
    }


    @Override
    public Target getOneLegalMove() {
        Target sat = null;
        sat = hasLegalMoveOneDirection(1, 1);
        if (sat != null) {
            return sat;
        }
        sat = hasLegalMoveOneDirection(1, -1);
        if (sat != null) {
            return sat;
        }
        sat = hasLegalMoveOneDirection(-1, 1);
        if (sat != null) {
            return sat;
        }
        sat = hasLegalMoveOneDirection(-1, -1);
        if (sat != null) {
            return sat;
        }
        return sat;
    }


}
