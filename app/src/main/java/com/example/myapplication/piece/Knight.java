package com.example.myapplication.piece;

import com.example.myapplication.chess.Move;
import com.example.myapplication.chess.Target;
import com.example.myapplication.model.Position;
import com.example.myapplication.model.Chessboard;
import com.example.myapplication.model.DirectionDistance;
import com.example.myapplication.model.Outcome;


/**
 *This class create a chess piece - Knight and defines the initial position and valid moves of knight.
 *
 * @author Cheng Peng
 * @author Nirjar Patel
 *
 */

public class Knight extends Piece {

    /**
     * @param isWhite Identifying the color of the piece
	 * @param file File of current location
     * @param rank Rank of current location
     * @param cb Chessboard
     */
    public Knight(boolean isWhite, int file, int rank,  Chessboard cb) {
        super(isWhite, file, rank, cb);
        cb.getPieceMap().put(getKey(), this);
    }


    /**
     * @param isWhite Identifying the color of the piece
     * @param fileRank Input File and Rank
     * @param cb Chessboard
     */
    public Knight(boolean isWhite, String fileRank, Chessboard cb) {
        super(isWhite, fileRank, cb);
        cb.getPieceMap().put(getKey(), this);
    }

	/**
	 * @return The board output for the Knight.
	 */
	@Override
	public String toString() {
		return super.toString() + "N";
	}



	/**
     * @param targetIndex Target indicating the target position of pieces
     * @param promotion Promotion. Pawn .
     * @return Result of the move
     */
    public Outcome doMove(Position targetIndex, Move.Promotion promotion) {
        return doMoveInternal(targetIndex, true);
    }


    /**
     * @param targetIndex Target indicating the target position of pieces
     * @return Result if the piece can attack target
     */
    public Outcome doAttack(Position targetIndex) {
        return doMoveInternal(targetIndex, false);
    }


    /**
     * @param targetIndex Target index a piece moves too
     * @param doMove True if piece perform move, false checks if piece can attack target
     * @return Outcome of the move
     */
    private Outcome doMoveInternal(Position targetIndex, boolean doMove) {
        Outcome outcome;
        //
        Position currentIndex = getPosition();
        //
        DirectionDistance dd = new DirectionDistance(currentIndex.fileIdx, currentIndex.rankIdx, targetIndex.fileIdx, targetIndex.rankIdx);
        //
        if (dd.isKnightly()) {
            if (isTargetEmptyOrLegal(targetIndex)) {
	        	if (doMove) {
	        		boolean isLegal = doActualMove(targetIndex, false);
	                if (isLegal) {
	                    outcome = new Outcome(true, "OK");
	                }
	                else {
	                    // King in check
	                    outcome = new Outcome(false, "Illegal move, try again");
	                }
	        	}
	        	else {
	                outcome = new Outcome(true, "OK");
	        	}
            }
            else {
                // Target piece is the same color
                outcome = new Outcome(false, "Illegal move, try again");
            }
        }
        else {
            outcome = new Outcome(false, "Illegal move, try again");
        }
        //
        return outcome;
    }


    /**
     * @return True if Knight has legal move. For checkmate and stalemate.
     */
    @Override
    public boolean hasLegalMove() {
		return hasLegalMoveOneDirectionOneStep(1, 2)!=null
                || hasLegalMoveOneDirectionOneStep(1, -2)!=null
                || hasLegalMoveOneDirectionOneStep(2, 1)!=null
                || hasLegalMoveOneDirectionOneStep(2, -1)!=null
                || hasLegalMoveOneDirectionOneStep(-1, 2)!=null
                || hasLegalMoveOneDirectionOneStep(-1, -2)!=null
                || hasLegalMoveOneDirectionOneStep(-2, 1)!=null
                || hasLegalMoveOneDirectionOneStep(-2, -1)!=null;
	}

	
	
    @Override
    public Target getOneLegalMove() {
    	Target sat = null;
    	//
    	sat = hasLegalMoveOneDirectionOneStep(1, 2);
    	if (sat!=null) {
    		return sat;
    	}
    	//
    	sat = hasLegalMoveOneDirectionOneStep(1, -2);
    	if (sat!=null) {
    		return sat;
    	}
    	//
    	sat = hasLegalMoveOneDirectionOneStep(2, 1);
    	if (sat!=null) {
    		return sat;
    	}
    	//
    	sat = hasLegalMoveOneDirectionOneStep(2, -1);
    	if (sat!=null) {
    		return sat;
    	}
    	//
    	sat = hasLegalMoveOneDirectionOneStep(-1, 2);
    	if (sat!=null) {
    		return sat;
    	}
    	//
    	sat = hasLegalMoveOneDirectionOneStep(-1, -2);
    	if (sat!=null) {
    		return sat;
    	}
    	//
    	sat = hasLegalMoveOneDirectionOneStep(-2, 1);
    	if (sat!=null) {
    		return sat;
    	}
    	//
    	sat = hasLegalMoveOneDirectionOneStep(-2, -1);
    	if (sat!=null) {
    		return sat;
    	}
    	//
    	return sat;
    }

}